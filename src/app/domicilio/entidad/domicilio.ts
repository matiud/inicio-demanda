import { Localidad } from 'src/app/localidad/entidad/localidad';

export class Domicilio {
    calle: string;
    nro: number;
    piso: number;
    dpto: string;
    localidad: Localidad;
}
