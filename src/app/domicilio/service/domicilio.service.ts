import { Injectable } from '@angular/core';
import { Domicilio } from '../entidad/domicilio';
import { GenericService, LoadService, MessageService } from '@jussanjuan/angular-pjsj';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DomicilioService  extends GenericService <Domicilio, Domicilio>{
  url: string;

  constructor(httpClient: HttpClient, loadService: LoadService, messageService: MessageService) {
    super(httpClient, loadService, messageService);
    this.url = '/domicilio'
  }
}
