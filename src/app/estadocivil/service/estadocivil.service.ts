import { Injectable } from '@angular/core';
import { Estadocivil } from '../entidad/estadocivil';
import { GenericService, LoadService, MessageService } from '@jussanjuan/angular-pjsj';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EstadocivilService extends GenericService <Estadocivil, Estadocivil>{
  url: string;

  constructor(httpClient: HttpClient, loadService: LoadService, messageService: MessageService) {
    super(httpClient, loadService, messageService);
    this.url = '/tipo-estado-civil'
  }
}
