import { Component, OnInit } from '@angular/core';
import { AuthPignusService } from '@jussanjuan/angular-pjsj';
import { HomeConfig } from '@jussanjuan/angular-pjsj/lib/domain/generic/home-config';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  configuracionDeHome: HomeConfig = {
    appName: 'Sistema de Expediente Electrónico - PJSJ',
    linksMenu: []
  };

  constructor(
    private authService: AuthPignusService,
  ) { }

  ngOnInit(): void {


   
    this.configuracionDeHome.linksMenu = [
      {
        name: 'Inicios de demanda',
        path: 'inicio-demanda',
        icon: 'icono-arg-datos-abiertos-2',
        levels: []
      }
     ];
  }

  logout() {
    console.log("logout");
    this.authService.logOut();

    // urlLogin es la url de login de Pignus
    window.location.href = environment.urlLogin;
  }

}
