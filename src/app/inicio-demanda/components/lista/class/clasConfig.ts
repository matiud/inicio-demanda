export class classHeader{
    name:string
}

export class classItem{
  name:string
  vinculo:string
  dni:string
  // naturaleza:string
}
export class classAction{
  name:string
}

export class classConfig{
  title:String;
  headers:classHeader[];
  items:classItem[];
  actions:classAction[]
}