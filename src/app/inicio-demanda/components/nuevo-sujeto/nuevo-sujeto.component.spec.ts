import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoSujetoComponent } from './nuevo-sujeto.component';

describe('NuevoSujetoComponent', () => {
  let component: NuevoSujetoComponent;
  let fixture: ComponentFixture<NuevoSujetoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NuevoSujetoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoSujetoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
