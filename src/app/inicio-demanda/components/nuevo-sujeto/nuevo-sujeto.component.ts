import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { LoadService } from '@jussanjuan/angular-pjsj';
import { PersonaService } from 'src/app/persona/service/persona.service';
import { classConfig, classItem } from '../lista/class/clasConfig';

@Component({
  selector: 'app-nuevo-sujeto',
  templateUrl: './nuevo-sujeto.component.html',
  styleUrls: ['./nuevo-sujeto.component.scss']
})
export class NuevoSujetoComponent implements OnInit {
  @Output() cancelar = new EventEmitter<{}>();
  @Output() save = new EventEmitter<{}>();
  tiposPersona = []
  tiposPartes= []

  item: classItem

  sujetoForm = new FormGroup({
      tipoParte: new FormControl('',Validators.required),
      persona: new FormControl('',Validators.required),
      tipoPersona : new FormControl('',Validators.required),
      representantes: this.formBuilder.array([]),
      item : new FormControl('')
    });
  constructor(private formBuilder:FormBuilder,private servicePersona: PersonaService,private loadService:LoadService) { }


  ngOnInit(): void {
    this.cargaTiposPersona()
    this.cargaTipoParte()
     this.servicePersona.findAll({ p: 0, s: 100 })
  }
  cargaTiposPersona(){
    this.tiposPersona.push({id:9,name:'hola'},{id:8,name:'chau'},{id:6,name:'Good morning'},{id:5,name:'Hello'},{id:1,name:'hola'})
    console.log(this.tiposPersona)
  }

  cargaTipoParte(){
    this.tiposPartes.push({id:1,name:'ACTOR'})
  }

  cancelaCarga(){
    this.cancelar.emit({nuevSujeto:false})
  }
  guradar(){
    this.item.dni = this.sujetoForm.get('persona').value.dni
    this.item.name = this.sujetoForm.get('persona').value.denominacion
     this.sujetoForm.get('item').setValue(this.item)
    this.save.emit()
  }

  ngAfterViewInit(): void {
    this.loadService.hideLoadGeneralLoad()
  }


  

}
