import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StepperProcesoJudicialComponent } from './stepper-proceso-judicial.component';

describe('StepperProcesoJudicialComponent', () => {
  let component: StepperProcesoJudicialComponent;
  let fixture: ComponentFixture<StepperProcesoJudicialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StepperProcesoJudicialComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StepperProcesoJudicialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
