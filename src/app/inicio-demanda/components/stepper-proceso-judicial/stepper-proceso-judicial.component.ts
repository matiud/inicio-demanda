import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { LoadService } from '@jussanjuan/angular-pjsj';
import { classConfig } from '../lista/class/clasConfig';

@Component({
  selector: 'app-stepper-proceso-judicial',
  templateUrl: './stepper-proceso-judicial.component.html',
  styleUrls: ['./stepper-proceso-judicial.component.scss']
})
export class StepperProcesoJudicialComponent implements OnInit {

  @Input() tipoProceso:any 

  @Output() action = new EventEmitter<{}>();

  firtFormGroup = new FormGroup({});

  partes = new FormGroup({
    partesItemsArray: this.formBuilder.array([])
      });
  
  secondFormGroup = new FormGroup({
   
    })

  threeFormGroup = new FormGroup({
    tecCtr: new FormControl('',Validators.required),
        });
  dataList=new classConfig()
  isLinear = false;
  
  nuevSujeto = false;

  constructor(private formBuilder:FormBuilder,private loadService: LoadService) { }

  ngOnInit(): void {
       this.cargaLista()
       this.creaFormEjecucionesFiscales()

  }

  creaFormEjecucionesFiscales(){
    this.firtFormGroup = this.formBuilder.group({
      caratulaTentativa: new FormControl('', Validators.required),
      montoDemanda : new FormControl('',[Validators.required,Validators.min(0)])
    })
    this.secondFormGroup = this.formBuilder.group({
     
    })
    this.threeFormGroup = this.formBuilder.group({})
    
  }

  cargaLista(){
    this.dataList.title = 'Lista de Partes cargadas'
    this.dataList.headers=[{name:'Vinculo'},{name:'Apellido y Nombre/ Denominacion'},{name:'Documento/ Numero'}]
    this.dataList.items = [
      {
        name:'asdsad',
        vinculo:'Autor',
        dni:'asdsads',
        // naturaleza:'asdsad'
  
      },
      {
        name:'asdsad',
        vinculo:'Autor',
        dni:'asdsads',
        // naturaleza:'asdsad'
  
      },
      {
        name:'asdsad',
        vinculo:'Autor',
        dni:'asdsads',
        // naturaleza:'asdsad'
  
      },
      {
        name:'asdsad',
        vinculo:'Autor',
        dni:'asdsads',
        // naturaleza:'asdsad'
  
      },
      {
        name:'asdsad',
        vinculo:'Autor',
        dni:'asdsads',
        // naturaleza:'asdsad'
  
      },
      {
        name:'asdsad',
        vinculo:'Autor',
        dni:'asdsads',
        // naturaleza:'asdsad'
  
      },
      {
        name:'asdsad',
        vinculo:'Autor',
        dni:'asdsads',
        // naturaleza:'asdsad'
  
      },{
        name:'asdsad',
        vinculo:'Autor',
        dni:'asdsads',
        // naturaleza:'asdsad'
  
      },
      {
        name:'asdsad',
        vinculo:'Autor',
        dni:'asdsads',
        // naturaleza:'asdsad'
  
      }
    ]
    
   
  }

  ngAfterViewInit(): void {
    this.loadService.hideLoadGeneralLoad()
  }

  volverInicioDemanda(){
   this.action.emit({inicioDemanda:false})
  }



  deleteItemList(event){
    this.dataList.items.splice(event.item,1)
  }

  nuevoSujeto(){
    this.nuevSujeto =true
  }

  //hijo nuevo-sujeto
  cancelarCargaParte(event){
    console.log(event)
    this.nuevSujeto = event.nuevSujeto
   }

   addSujeto(event){
    this.dataList.items.push(event.item)
   }



}
