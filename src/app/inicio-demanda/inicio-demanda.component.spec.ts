import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InicioDemandaComponent } from './inicio-demanda.component';

describe('InicioDemandaComponent', () => {
  let component: InicioDemandaComponent;
  let fixture: ComponentFixture<InicioDemandaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InicioDemandaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InicioDemandaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
