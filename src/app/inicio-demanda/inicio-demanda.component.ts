
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthPignusService, LoadService } from '@jussanjuan/angular-pjsj';

@Component({
  selector: 'app-inicio-demanda',
  templateUrl: './inicio-demanda.component.html',
  styleUrls: ['./inicio-demanda.component.scss']
})
export class InicioDemandaComponent implements OnInit {


  
  // datos de tipo de causa
  tipoCausaForm = new FormGroup({
    fuero: new FormControl('',Validators.required),
    procesoJudicial: new FormControl('',Validators.required),
    oficinaJudiial: new FormControl('',Validators.required),
    });
    
  fueros=[{
    name:'Civil',
    id:1}
  ]
  procesosJudiciales = [{
    name:'proceso',
    id:1}
  ]
  oficinasJudiciales = [{
    name:'oficina',
    id:1}
  ]

  inicioDemanda=false;
    
  constructor(
    private loadService: LoadService
  ) { }


  ngOnInit(): void {
    
  } 

  ngAfterViewInit(): void {
    this.loadService.hideLoadGeneralLoad()
  }

  iniciarDemanda(){
    this.inicioDemanda = this.tipoCausaForm.get('procesoJudicial').value;
  }
  volverInicioDemanda(event){
    this.inicioDemanda = event.inicioDemanda;
  }

}
