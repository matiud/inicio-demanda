import { Provincia } from 'src/app/provincia/entidad/provincia';

export class Localidad {
    id: number;
    nombre: string;
    provincia: Provincia;
}
