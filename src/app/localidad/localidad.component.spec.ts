import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LocalidadComponent } from './localidad.component';

describe('LocalidadComponent', () => {
  let component: LocalidadComponent;
  let fixture: ComponentFixture<LocalidadComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ LocalidadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
