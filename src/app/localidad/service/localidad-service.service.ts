import { Injectable } from '@angular/core';
import { GenericService, MessageService, LoadService, ResponseResult } from '@jussanjuan/angular-pjsj';
import { HttpClient } from '@angular/common/http';
import {Localidad} from '../entidad/localidad';

@Injectable({
  providedIn: 'root'
})
export class LocalidadServiceService extends GenericService<Localidad, Localidad>{
  url: string;

  constructor(httpClient: HttpClient, loadService: LoadService, messageService: MessageService) {
    super(httpClient, loadService, messageService);
    this.url = 'api-pg/localidad'
  }
  
}
