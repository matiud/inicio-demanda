import { Injectable } from '@angular/core';
import { GenericService, MessageService, LoadService, ResponseResult } from '@jussanjuan/angular-pjsj';
import {Localidad} from '../entidad/localidad';
import { HttpClient } from '@angular/common/http';
import { of, Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class LocalidadService extends GenericService<Localidad, Localidad>{
  url: string;

  constructor(httpClient: HttpClient, loadService: LoadService, messageService: MessageService) {
    super(httpClient, loadService, messageService);
    this.url = '/localidad'
  }
  
}
