import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthPignusService, LoadService, PignusLogin } from '@jussanjuan/angular-pjsj';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent extends PignusLogin {

  constructor(  authService: AuthPignusService,
    private router: Router,
   activatedRoute: ActivatedRoute) {
    super(authService, activatedRoute);
  }
  /**
   * Este metodo se invoca cuando detecta un acceso no permitido a la plataforma.
   * Desde este espacio se debe siempre redirigir a la ruta de login de pignus, pero tambien se pueden ejecutar
   * validaciones de datos, generar carteles, etc.
   * Pero sin redirigir a Pignus no se va a poder utilizar las acciones que sean privadas en un servidor
   *
   */
  public redirectNotAuth() {
    console.log(LoginComponent.name + ": No autenticado");
    // urlLogin es la url de login de Pignus
    window.location.href = environment.urlLogin;
  }
  /**
   * Este metodo se invoca cuando se autentica correctamente con pignus.
   * Desde este espacio se pueden ejecutar diferentes tipos de redirecciones, validaciones y demás.
   *
   */
  public successAuth() {
    console.log(LoginComponent.name + ": Autenticado");
    // Cuando se autentique correctamente, es necesario redirigirlo a la ruta que intentaba acceder. O en esta caso siempre al home
    this.router.navigate(['/home']);
  }

}
