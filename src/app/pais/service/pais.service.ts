import { Injectable } from '@angular/core';
import { Pais } from '../entidad/pais';
import { GenericService, LoadService, MessageService } from '@jussanjuan/angular-pjsj';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PaisService extends GenericService <Pais, Pais> {
  url: string;

  constructor(httpClient: HttpClient, loadService: LoadService, messageService: MessageService) {
    super(httpClient, loadService, messageService);
    this.url = '/pais'
  }
}
