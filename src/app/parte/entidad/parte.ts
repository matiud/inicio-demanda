import { ClassConfig, FieldType } from '@jussanjuan/angular-pjsj';

export class Parte {
    id: number | undefined;
    fechaCreacion: Date | undefined;
    idExpediente: number | undefined;
    idPersona: number | undefined;
    idTipoParte: number | undefined;
    nombreParte: String | undefined;
    nombrePersona: String | undefined;

    public static getClassConfig(): ClassConfig {
        return {
            entityName: 'Parte',
            idKeys: ['id'],
            fieldConfig: [{
                key: 'id',
                display: 'Código',
                isColumn: true,
                isFilter: true,
                typeFilter: FieldType.inputNumber
            }, {
                key: 'idExpediente',
                display: 'Expediente',
                isColumn: false,
                isFilter: false,
                typeFilter: FieldType.inputNumber
            },  {
                key: 'idTipoParte',
                display: 'Tipo parte',
                isColumn: false,
                isFilter: false,
                typeFilter: FieldType.inputNumber
            }, {
                key: 'nombreParte',
                display: 'Parte',
                isColumn: true,
                isFilter: true,
                typeFilter: FieldType.inputText
            },{
                key: 'nombrePersona',
                display: 'Nombre',
                isColumn: true,
                isFilter: true,
                typeFilter: FieldType.inputText
            }]
        };
      }

     
}
