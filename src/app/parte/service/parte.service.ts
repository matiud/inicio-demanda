import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GenericService, LoadService, MessageService } from '@jussanjuan/angular-pjsj';
import { Parte } from '../entidad/parte';

@Injectable({
  providedIn: 'root'
})
export class ParteService extends GenericService <Parte, Parte>{
  url: string;

  constructor(httpClient: HttpClient, loadService: LoadService, messageService: MessageService) {
    super(httpClient, loadService, messageService);
    this.url = '/parte'
  }
}
