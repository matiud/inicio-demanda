import { Sexo } from 'src/app/sexo/entidad/sexo';
import { Estadocivil } from 'src/app/estadocivil/entidad/estadocivil';
import { Tipodocumento } from 'src/app/tipodocumento/entidad/tipodocumento';
import { Domicilio } from 'src/app/domicilio/entidad/domicilio';
import { ClassConfig, FieldType } from '@jussanjuan/angular-pjsj';

export class Persona {
	id: number;
	nroTelefono: number;
	mail: string;
  domicilioActual: Domicilio;
  denominacion: String;
  tipo: String;

	public static getClassConfig(): ClassConfig {
        return {
            entityName: 'Persona',
            idKeys: ['id'],
            fieldConfig: [{
            key: 'id',
            display: 'Código',
            isColumn: true,
            isFilter: true,
            typeFilter: FieldType.inputNumber
          },{
            key: 'mail',
            display: 'Mail',
            isColumn: true,
            isFilter: true,
            typeFilter: FieldType.inputText
          },{
            key: 'nroTelefono',
            display: 'Telefono',
            isColumn: true,
            isFilter: true,
            typeFilter: FieldType.inputText
          },{
            key: 'denominacion',
            display: 'Denominacion',
            isColumn: true,
            isFilter: true,
            typeFilter: FieldType.inputText
          }]
        };
      }
}
