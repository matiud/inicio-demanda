import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { ActionConfig, ClassConfig, GTableComponent, LoadService, OrderDirection } from '@jussanjuan/angular-pjsj';
import { AltaPersonaComponent } from './alta-persona/alta-persona.component';
import { Persona } from './entidad/persona';
import { PersonaService } from './service/persona.service';

@Component({
  selector: 'app-persona',
  templateUrl: './persona.component.html',
  styleUrls: ['./persona.component.scss']
})
export class PersonaComponent implements OnInit {
  @ViewChild("tablePersona") table: GTableComponent;
  classConfig: ClassConfig = Persona.getClassConfig();
  
  actions: ActionConfig[];
  actionsFilter: ActionConfig[];
  where = new Map<string,any>();
  orderBy = new Map();
  constructor(private loadService: LoadService,
    private http: HttpClient,
    public router: Router,
    public personaService: PersonaService,
    private dialog: MatDialog,
    private _snackBar: MatSnackBar) { 
      let actionEdit = new ActionConfig();
      let actionAdd = new ActionConfig();
      actionEdit.event = new EventEmitter();
      actionEdit.text = 'Gestionar';
      actionAdd.event = new EventEmitter();
      actionAdd.text = 'Nueva persona';
      actionAdd.event.subscribe((persona) => {
        this.addPersona(persona);
      });
      // Se subscribe la funcion que se va a ejecutar al click del boton
      /*actionEdit.event.subscribe((inicioDemanda) => {
        this.verDetalles(inicioDemanda);
      });*/
      this.actions = [actionEdit];
      this.actionsFilter = [actionAdd];
      this.orderBy.set("id", OrderDirection.DESC);
    }

  ngOnInit() {
  }

  addPersona(persona){
      console.log("Añadir persona");
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.width = '700px';
      //dialogConfig.height = '600px';
      /*dialogConfig.data = {
        expediente: this.route.snapshot.params.id
      }*/

      const dialogRef = this.dialog.open(AltaPersonaComponent, dialogConfig);

      dialogRef.afterClosed().subscribe(result => {
        this.table.refresh();
      });
  }

}
