import { Injectable } from '@angular/core';
import { GenericService, MessageService, LoadService, ResponseResult } from '@jussanjuan/angular-pjsj';
import {Persona} from '../entidad/persona';
import { HttpClient } from '@angular/common/http';
import { of, Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class PersonaService extends GenericService<Persona, Persona> {
  url: string;

  constructor(httpClient: HttpClient, loadService: LoadService, messageService: MessageService) {
    super(httpClient, loadService, messageService);
    this.url = '/persona'
  }

}
