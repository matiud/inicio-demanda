import { Pais } from 'src/app/pais/entidad/pais';

export class Provincia {
    id: number;
    nombre: string;
    pais: Pais;
}
