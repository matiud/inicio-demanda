import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoadService, MessageService, GenericService } from '@jussanjuan/angular-pjsj';
import { Provincia } from '../entidad/provincia';

@Injectable({
  providedIn: 'root'
})
export class ProvinciaService extends GenericService <Provincia, Provincia> {
  url: string;

  constructor(httpClient: HttpClient, loadService: LoadService, messageService: MessageService) {
    super(httpClient, loadService, messageService);
    this.url = '/provincia'
  }
  
}
