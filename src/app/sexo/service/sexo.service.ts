import { Injectable } from '@angular/core';
import { Sexo } from '../entidad/sexo';
import { GenericService, LoadService, MessageService } from '@jussanjuan/angular-pjsj';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SexoService extends GenericService <Sexo, Sexo>{
  url: string;

  constructor(httpClient: HttpClient, loadService: LoadService, messageService: MessageService) {
    super(httpClient, loadService, messageService);
    this.url = '/tipo-sexo'
  }
}
