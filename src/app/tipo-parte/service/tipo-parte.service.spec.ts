import { TestBed } from '@angular/core/testing';

import { TipoParteService } from './tipo-parte.service';

describe('TipoParteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TipoParteService = TestBed.get(TipoParteService);
    expect(service).toBeTruthy();
  });
});
