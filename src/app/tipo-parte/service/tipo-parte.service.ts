import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GenericService, LoadService, MessageService } from '@jussanjuan/angular-pjsj';
import { TipoParte } from '../entidad/tipo-parte';

@Injectable({
  providedIn: 'root'
})
export class TipoParteService extends GenericService <TipoParte, TipoParte>{
  url: string;

  constructor(httpClient: HttpClient, loadService: LoadService, messageService: MessageService) {
    super(httpClient, loadService, messageService);
    this.url = '/tipo-parte'
  }
}
