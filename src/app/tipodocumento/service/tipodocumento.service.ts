import { Injectable } from '@angular/core';
import { Tipodocumento } from '../entidad/tipodocumento';
import { GenericService, LoadService, MessageService } from '@jussanjuan/angular-pjsj';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TipodocumentoService extends GenericService <Tipodocumento, Tipodocumento>{
  url: string;

  constructor(httpClient: HttpClient, loadService: LoadService, messageService: MessageService) {
    super(httpClient, loadService, messageService);
    this.url = '/tipo-documento'
  }
}
