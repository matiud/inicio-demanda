import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TipodocumentoComponent } from './tipodocumento.component';

describe('TipodocumentoComponent', () => {
  let component: TipodocumentoComponent;
  let fixture: ComponentFixture<TipodocumentoComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TipodocumentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipodocumentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
